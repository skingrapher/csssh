#!/usr/bin/awk -f
#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright © 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
# adaptation for awk from
# https://github.com/matthiasmullie/minify/blob/master/src/CSS.php
@include "trim.awk"
function minifier(text) {
    # trim whitespaces
    text = trim(text)
    # strip comments
    gsub(/\/\*.*?\*\//, "", text)
    #remove empty tags
    gsub(/^[^\{\};]+\{[[:space:]]\}/, "", text)
    text = gensub(/(\}|;)[^\{\};]+\{[[:space:]]\}/, "\\1", "g", text)
    # remove whitespace around meta characters
    # https://stackoverflow.com/questions/15195750/minify-compress-css-with-regex
    text = gensub(/[[:space:]]*([\*$~^|]?+=|[{};,>~]|!important\b)[[:space:]]/, "\\1", "g", text)
    text = gensub(/([\[\(:>\+])[[:space:]]+/, "\\1", "g", text)
    text = gensub(/[[:space:]]+([\]\)>\+])/, "\\1", "g", text)
    gsub(/[[:space:]]+:/, ":", text)
    gsub(/:[[:space:]]+/, ":", text)
    # strip units after zero
    text = gensub(/([:\(, ])-?0(\.0+)?(em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|vmax|vm)([ ,\);\}])/, "\\10\\4", "g", text)
    # strip trailing zero: .0 -> 0
    text = gensub(/([:\(, ])\.0*(em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|vmax|vm)?([ ,\);\}])/, "\\10\\3", "g", text)
    # strip trailing zero (50.10 -> 50.1)
    text = gensub(/([:\(, ])(-?[0-9]+\.[1-9]+)0+(em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|vmax|vm)?([ ,\);\}])/, "\\1\\2\\3\\4", "g", text)
    # strip trailing zero (50.00 -> 50)
    text = gensub(/([:\(, ])(-?[0-9]+)\.0+(em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|vmax|vm)?([ ,\);\}])/, "\\1\\2\\3\\4", "g", text)
    # strip leading zero (0.8 -> .8, 01.1 -> 1.1)
    text = gensub(/([:\(, ])-?0+([0-9]*\.[0-9]+)(em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|vmax|vm)?([ ,\);\}])/, "\\1\\2\\3\\4", "g", text)

    return text
    }

function minify(text, buffer) {
    
    if (match(text, /calc\(/) == 0)
        return sprintf("%s%s", buffer, minifier(text))
    else
        patsplit(text, token, /calc\(.*\)/)
        for (t in token) {
            buffer = sprintf("%s%s%s", buffer, minifier(token[t]), seps[t])
            print buffer
    }
        return buffer
    }

BEGIN {
    buffer = ""
    }
# strip blank lines
/^$/ { next }
{
    buffer = minify($0,buffer)
    1
    }
/:nth-(last-)*(of-)*(child|type)/ {
    # whitespace around + and - can only be stripped
    # inside some pseudo-classes like `:nth-child(3+2n)`
    # not in things like `calc(3px + 2px)`,
    # shorthands like `3px -2px`, or
    # selectors like `div.weird- p`
    buffer = gensub(/:(nth-child|nth-last-child|nth-last-of-type|nth-of-type)\([[:space:]]*([+-]?)[[:space:]]*(.+?)[[:space:]]*([+-]?)[[:space:]]*(.*?)[[:space:]]\)/, ":\\1(\\2\\3\\4\\5)", "g", buffer)
    1
    }
/:[[:space:]]*(#[a-fA-F0-9]{3,6}|white|black)/ {
    # shorten some colors
    gsub("#f0ffff", "azure", buffer)
    gsub("#f5f5dc", "beige", buffer)
    gsub("#a52a2a", "brown", buffer)
    gsub("#ff7f50", "coral", buffer)
    gsub("#ffd700", "gold", buffer)
    gsub("#808080", "gray", buffer)
    gsub("#008000", "green", buffer)
    gsub("#4b0082", "indigo", buffer)
    gsub("#fffff0", "ivory", buffer)
    gsub("#f0e68c", "khaki", buffer)
    gsub("#faf0e6", "linen", buffer)
    gsub("#800000", "maroon", buffer)
    gsub("#000080", "navy", buffer)
    gsub("#808000", "olive", buffer)
    gsub("#cd853f", "peru", buffer)
    gsub("#ffc0cb", "pink", buffer)
    gsub("#dda0dd", "plum", buffer)
    gsub("#800080", "purple", buffer)
    gsub("#f00", "red", buffer)
    gsub("#fa8072", "salmon", buffer)
    gsub("#a0522d", "sienna", buffer)
    gsub("#c0c0c0", "silver", buffer)
    gsub("#fffafa", "snow", buffer)
    gsub("#d2b48c", "tan", buffer)
    gsub("#ff6347", "tomato", buffer)
    gsub("#ee82ee", "violet", buffer)
    gsub("#f5deb3", "wheat", buffer)
    gsub("#ffffff", "#fff", buffer)
    gsub("#000000", "#000", buffer)
    # uppercase
    gsub("#F0FFFF", "azure", buffer)
    gsub("#F5F5DC", "beige", buffer)
    gsub("#A52A2A", "brown", buffer)
    gsub("#FF7F50", "coral", buffer)
    gsub("#FFD700", "gold", buffer)
    gsub("#4B0082", "indigo", buffer)
    gsub("#FFFFF0", "ivory", buffer)
    gsub("#F0E68C", "khaki", buffer)
    gsub("#FAF0E6", "linen", buffer)
    gsub("#CD853F", "peru", buffer)
    gsub("#FFC0CB", "pink", buffer)
    gsub("#DDA0DD", "plum", buffer)
    gsub("#F00", "red", buffer)
    gsub("#FA8072", "salmon", buffer)
    gsub("#A0522D", "sienna", buffer)
    gsub("#C0C0C0", "silver", buffer)
    gsub("#FFFAFA", "snow", buffer)
    gsub("#D2B48C", "tan", buffer)
    gsub("#FF6347", "tomato", buffer)
    gsub("#EE82EE", "violet", buffer)
    gsub("#F5DEB3", "wheat", buffer)
    gsub("#FFFFFF", "#fff", buffer)
    gsub(/:[[:space:]]*white/, ":#fff", buffer)
    gsub(/:[[:space:]]*black/, ":#000", bu00er)
    1
    }
END {
    print buffer
    }


