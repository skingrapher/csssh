#!/usr/bin/awk -f

#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright � 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
@include "trim.awk"
@include "selector.awk"
function clip(d, min, max) {
    if (d < min) { return min }
    else if (d > max) { return max }
    else { return d }
    }

function abs(v) { return (v < 0)? -v: v }

function max(n0,n1) {
    if (n0 < n1) { return n1 }
    else { return n0 }
    }

function min(n0,n1) {
    if (n0 > n1) { return n1 }
    else { return n0 }
    }

function hex2dec(w) {
    if (match(w, /[aA]/)) {return 10}
    else if (match(w, /[bB]/)) { return 11 }
    else if (match(w, /[cC]/)) { return 12 }
    else if (match(w, /[dD]/)) { return 13 }
    else if (match(w, /[eE]/)) { return 14 }
    else if (match(w, /[fF]/)) { return 15 }
    else if (match(w, /[0-9]/)) { return w }
    }

function xy2d(x,y) {
    n = 16
    d = 0
    for (s=n/2; s>0; s/=2) {
        rx = and(x,s)
        ry = and(y,s)
        d += s * s* ((3 * rx) ^ ry)
        rot(n, x, y, rx, ry)
        }
    return int(d)
    }

# HILBER CURVE SORTING
# @source https://en.wikipedia.org/wiki/Hilbert_curve
function rot(n, x, y, rx, ry) {
    if (ry == 0) {
        if (rx == 1) {
            x = n - 1 - x
            y = n - 1 - y
            }
        }
        t = x
        x = y
        y = t
    }

function hilbertindex(hex) {
    sub("#", "", hex)
    if (length(hex) == 3) {
        r = hex2dec(substr(hex,1,1))
        g = hex2dec(substr(hex,2,1))
        b = hex2dec(substr(hex,3))
        _d[1] = xy2d(r,r)
        _d[2] = xy2d(g,g)
        _d[3] = xy2d(b,b)
        }
    else {
        r0 = hex2dec(substr(hex,1,1))
        r1 = hex2dec(substr(hex,2,1))
        g0 = hex2dec(substr(hex,3,1))
        g1 = hex2dec(substr(hex,4,1))
        b0 = hex2dec(substr(hex,5,1))
        b1 = hex2dec(substr(hex,6))
        _d[1] = xy2d(r0,r1)
        _d[2] = xy2d(g0,g1)
        _d[3] = xy2d(b0,b1)
        }
    return sprintf("%d %d %d", _d[1], _d[2], _d[3])
    }
# https://rosettacode.org/wiki/Sort_an_array_of_composite_structures#AWK
# https://www.gnu.org/software/gawk/manual/html_node/Controlling-Array-Traversal.html#Controlling-Array-Traversal
# https://www.gnu.org/software/gawk/manual/html_node/Array-Sorting-Functions.html
function cmp_hilbert(i1, v1, i2, v2) {
    # indexes
    hi1 = hilbertindex(v1)
    hi2 = hilbertindex(v2)
    if (hi1 < hi2)
        return -1
    return (v1 != v2)
    }

function show(arr) {
    PROCINFO["sorted_in"] = "cmp_hilbert"
    for (color in arr) {
        printf "\t'%s': %s,\n", color, arr[color]
        }
    PROCINFO["sorted_in"] = "@val_type_asc"
    }
# shorten channel from hexa color
function shrtrgb(rgb) {
        if (match(rgb, /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/, sh)) {
            _R_ = sh[1]
            _G_ = sh[2]
            _B_ = sh[3]
            r0 = substr(_R_, 1, 1)
            r1 = substr(_R_, 2)
            r = (r0 == r1) ? r0 : _R_
            g0 = substr(_G_, 1, 1)
            g1 = substr(_G_, 2)
            g = (g0 == g1) ? g0 : _G_
            b0 = substr(_B_, 1, 1)
            b1 = substr(_B_, 2)
            b = (b0 == b1) ? b0 : _B_
            if (length(r) == length(g) && length(r) == length(b))
                return sprintf("#%s%s%s", r, g, b)
            }
            else { return rgb }
        return rgb
    }
# RGB FUNCTIONS
function red(rgb) {
    if (match(rgb, /rgba*\(([[:digit:]]{1,3}),[:space:]*([[:digit:]]{1,3}),[:space:]*([[:digit:]]{1,3})(,[[:space:]]*[[:digit:]]*\.[[:digit:]]+)*\)/, c)) {
        return c[1]
        }
    else if (match(rgb, /#[0-9a-fA-F]{3,6}/)) {
        #removes #
        rgb = substr(rgb,2)
        l = length(rgb)
        if (l == 3) {
            # get red channel
            c0 = substr(rgb,1,1)
            # convert to decimal
            d0 = hex2dec(c0)
            channel = (d0 == 0) ? 0 : d0 * 16 + d0
            return clip(int(channel), 0, 255)
            }
        else {
            c0 = substr(rgb,1,1)
            c1 = substr(rgb,2,1)
            d0 = hex2dec(c0)
            d1 = hex2dec(c1)
            channel = d0 * 16 + d1
            return clip(int(channel), 0, 255)
            }
        }
    }

function green(rgb) {
    if (match(rgb, /rgba*\(([[:digit:]]{1,3}),[:space:]*([[:digit:]]{1,3}),[:space:]*([[:digit:]]{1,3})(,[[:space:]]*[[:digit:]]*\.[[:digit:]]+)*\)/, c)) {
        return c[2]
        }
    else if (match(rgb, /#[0-9a-fA-F]{3,6}/)) {
        #removes #
        rgb = substr(rgb,2)
        l = length(rgb)
        if (l == 3) {
            # get green channel
            c0 = substr(rgb,2,1)
            # convert to decimal
            d0 = hex2dec(c0)
            channel = (d0 == 0) ? 0 : d0 * 16 + d0
            return clip(int(channel), 0, 255)
            }
        else {
            c0 = substr(rgb,3,1)
            c1 = substr(rgb,4,1)
            d0 = hex2dec(c0)
            d1 = hex2dec(c1)
            channel = d0 * 16 + d1
            return clip(int(channel), 0, 255)
            }
        }
    }

function blue(rgb) {
    if (match(rgb, /rgba*\(([[:digit:]]{1,3}),[:space:]*([[:digit:]]{1,3}),[:space:]*([[:digit:]]{1,3})(,[[:space:]]*[[:digit:]]*\.[[:digit:]]+)*\)/, c)) {
        return c[3]
        }
    else if (match(rgb, /#[0-9a-fA-F]{3,6}/)) {
        #removes #
        rgb = substr(rgb,2)
        l = length(rgb)
        if (l == 3) {
            # get blue channel
            c0 = substr(rgb,3,1)
            # convert to decimal
            d0 = hex2dec(c0)
            channel = d0 * 16 + d0
            return clip(int(channel), 0, 255)
            }
        else {
            c0 = substr(rgb,5,1)
            c1 = substr(rgb,6,1)
            d0 = hex2dec(c0)
            d1 = hex2dec(c1)
            channel = d0 * 16 + d1
            return clip(int(channel), 0, 255)
            }
        }
    }

function rgb2hex(rgb) {
    _r = red(rgb)
    _g = green(rgb)
    _b = blue(rgb)

    # dec -> hexa
    rh = sprintf("%x",_r)
    gh = sprintf("%x",_g)
    bh = sprintf("%x",_b)
    
    # for #aaaaaa format
    # #aaa format do not need 0
    rh = (length(rh) == 1) ? "0" rh : rh
    gh = (length(gh) == 1) ? "0" gh : gh
    bh = (length(bh) == 1) ? "0" bh : bh

    hex = sprintf("#%s%s%s", rh, gh, bh)
    return shrtrgb(hex)
    }

function lrgb2hex(lr, lg, lb) {
    # standard RGB
    _r = srgb_companding(lr) * 255 
    _g = srgb_companding(lg) * 255
    _b = srgb_companding(lb) * 255
    
    _r = clip(int(_r),0,255)
    _g = clip(int(_g),0,255)
    _b = clip(int(_b),0,255)

    # dec -> hexa
    rh = sprintf("%x",_r)
    gh = sprintf("%x",_g)
    bh = sprintf("%x",_b)
    # for #aaaaaa format
    # #aaa format do not need 0
    rh = (_r < 10) ? "0" rh : rh
    gh = (_g < 10) ? "0" gh : gh
    bh = (_b < 10) ? "0" bh : bh

    hex = sprintf("#%s%s%s", rh, gh, bh)
    return shrtrgb(hex)
    }

function hsl2hex(hsl) {
    if (match(hsl, /hsla*\(([[:digit:]]{1,3}),[:space:]*([[:digit:]]{1,3})%,[:space:]*([[:digit:]]{1,3})%(,[[:space:]]*[[:digit:]]*\.[[:digit:]]+)*\)/, c)) {
        # see algo: https://en.wikipedia.org/wiki/HSL_and_HSV#HSL_to_RGB_alternative
        # hue between 0 and 360
        H = c[1]
        H = (H > 360) ? H -360 : (H < 0) ? H + 360 : H
        # saturation between 0 and 1
        S = c[2] / 100
        if (S == 0)
            return "#000"
        # lightness between 0 and 1
        L = c[3] / 100

        a = S * min(L, 1 - L) 
        k0 = (H / 30) % 12
        k8 = (8 + H / 30) % 12
        k4 = (4 + H / 30) % 12

        R = L - a * max(min(min(k0 -3, 9 - k0), 1) , -1)
        G = L - a * max(min(min(k8 -3, 9 - k8), 1) , -1)
        B = L - a * max(min(min(k4 -3, 9 - k4), 1) , -1)

        R = int(R)
        G = int(G)
        B = int(B)
        R = min(max(0, R * 255), 255)
        G = min(max(0, G * 255), 255)
        B = min(max(0, B * 255), 255)

        # dec -> hexa (string)
        rh = sprintf("%x",R)
        gh = sprintf("%x",G)
        bh = sprintf("%x",B)
        # for #aaaaaa format
        # #aaa format do not need 0
        rh = (R < 10) ? "0" rh : rh
        gh = (G < 10) ? "0" gh : gh
        bh = (B < 10) ? "0" bh : bh
        hex = sprintf("#%s%s%s", rh, gh, bh)
        return shrtrgb(hex)
        }
    }

#CIE constants
function kappa() {
    # CIE standard
    #return 903.2962962962963
    # CIE standard optimization 
    return 24389/27  
}

function epsilon() {
    # CIE standard
    #return 0.0088564516790356308
    # CIE standard optimization 
    return 216/24389
}

# GAMMA
# sRGB_gamma = 2.4
# effective_gamma = 2.2
function gamma() {
    return 2.4
}

function pi() {
    return 3.14159265358979323846264338327950288419
}

# gamma correction
function inverse_srgb_companding(channel) {
    return (channel <= 0.04045) ? channel / 12.92 : ( (channel + 0.055) / 1.055 ) ^ gamma() 
}

function srgb_companding(channel) {
    inv_gamma = 1 / gamma()
    return  (channel <= 0.0031308) ? channel * 12.92 : (channel ^ inv_gamma) * 1.055 - 0.055
}

function infinite_slope_prevention(n) {
    return ( n > epsilon() ) ? n ^ (1/3) : (n * kappa() + 16) / 116
}

function reverse_infinite_slope_prevention(n) {
    return (n > epsilon()) ? n ^ 3 : (116 * n - 16) / kappa()
}

function dot_product(a,b) {
    ret = 0
    for (i in a) 
        ret += a[i] * b[i]
    return ret
}

function hex2lab(hex) {
    #removes #
    hex = substr(hex,2)
    l = length(hex)
    if (l == 3) {
        r = hex2dec(substr(hex,1,1))
        R = r * 16 + r
        g = hex2dec(substr(hex,2,1))
        G = g * 16 + g
        b = hex2dec(substr(hex,3))
        B = b * 16 + b
        }
    else {
        r0 = hex2dec(substr(hex,1,1))
        r1 = hex2dec(substr(hex,2,1))
        g0 = hex2dec(substr(hex,3,1))
        g1 = hex2dec(substr(hex,4,1))
        b0 = hex2dec(substr(hex,5,1))
        b1 = hex2dec(substr(hex,6))
        R = r0 * 16 + r1
        G = g0 * 16 + g1
        B = b0 * 16 + b1
        }

    R = R / 255
    G = G / 255
    B = B / 255

    # sRGB -> XYZ matrix
    X[1] = 0.4124564
    X[2] = 0.3575761
    X[3] = 0.1804375

    Y[1] = 0.2126729
    Y[2] = 0.7151522
    Y[3] = 0.072175

    Z[1] = 0.0193339
    Z[2] = 0.119192
    Z[3] = 0.9503041

    # linear rgb
    lrgb[1] = inverse_srgb_companding(R)
    lrgb[2] = inverse_srgb_companding(G)
    lrgb[3] = inverse_srgb_companding(B)

    _x = dot_product(X, lrgb)
    _y = dot_product(Y, lrgb)
    _z = dot_product(Z, lrgb)
    _x = clip(_x,0,1)
    _y = clip(_y,0,1)
    _z = clip(_z,0,1)

    delete lrgb

    xr = _x / reference_white_D65["x"]
    yr = _y / reference_white_D65["y"]
    zr = _z / reference_white_D65["z"]

    fx = infinite_slope_prevention(xr)
    fy = infinite_slope_prevention(yr)
    fz = infinite_slope_prevention(zr)

    # http://www.tsi.enst.fr/pages/enseignement/ressources/beti/correl_couleur/lab.html
    l = (yr > epsilon()) ? 116 * (yr ^ (1/3)) - 16 : kappa() * yr
    a = 500 * (fx - fy)
    b = 200 * (fy - fz)

    l = clip(l,0,100)
    a = clip(a,-128,127)
    b = clip(b,-128,127)

    return sprintf("%.4f %.4f %.4f", l, a, b)
    }

# dE value     | perception
# ------------------------------------
# 0 < dE < 1            invisible difference
# 1 < dE < 2            perceptible difference through close observation
# 2 < dE < 3.5          perceptible difference at a glance
# 3.5 < dE < 5          obvious difference in color
# 5 < dE < 10           observer notices two different colors
# 10 < dE < 50          colors are more similar than opposite
# 100                   colors are exact opposite
#
# samp: sample LAB color (string "%s %s %s")
# ref: reference LAB color (string "%s %s %s")
function dE00(samp, ref) {

    k_L = 1
    k_C = 1
    k_H = 1

    split(samp, labS, " ")
    split(ref, labR, " ")
    L_s = labS[1]
    a_s = labS[2]
    b_s = labS[3]
    L_r = labR[1]
    a_r = labR[2]
    b_r = labR[3]

    mean_L_prime = (L_s + L_r) / 2

    chroma_s = sqrt( (a_s ^ 2) + (b_s ^ 2) )
    chroma_r = sqrt( (a_r ^ 2) + (b_r ^ 2) )
    mean_chroma = (chroma_s + chroma_r) / 2

    _gamma = ( 1 - sqrt( (mean_chroma ^ 7) / ( (mean_chroma ^ 7) + (25 ^ 7) ) ) ) / 2

    a_prime_s = a_s * (1 + _gamma)
    a_prime_r = a_r * (1 + _gamma)

    C_prime_s = sqrt( (a_prime_s ^ 2) + (b_s ^ 2) )
    C_prime_r = sqrt( (a_prime_r ^ 2) + (b_r ^ 2) )

    mean_C_prime = (C_prime_s + C_prime_r) / 2

    ## h_prime_s = ((b_s / a_prime_s) >= 0) ? atan2(b_s, a_prime_s) : atan2(b_s, a_prime_s) + 360
    ## h_prime_r = ((b_r / a_prime_r) >= 0) ? atan2(b_r, a_prime_r) : atan2(b_r, a_prime_r) + 360
    h_prime_s = atan2(b_s, a_prime_s) % 360
    h_prime_r = atan2(b_r, a_prime_r) % 360

    mean_H_prime = (abs(h_prime_s - h_prime_r) <= 180) ? (h_prime_r + h_prime_s) / 2 : (h_prime_s + h_prime_r - 360) / 2

    theta = 1 - 0.17 * cos(mean_H_prime - 30) + 0.24 * cos(2 * mean_H_prime) + 0.32 * cos(3 * mean_H_prime + 6) - 0.2 * cos(4 * mean_H_prime - 63)

    delta_h_prime = (abs(h_prime_r - h_prime_s) <= 180) ? h_prime_r - h_prime_s : (abs(h_prime_r - h_prime_s) > 180 && h_prime_r <= h_prime_s) ? h_prime_r - h_prime_s + 360 : h_prime_r - h_prime_s - 360 

    delta_L_prime = L_r - L_s
    delta_C_prime = C_prime_r - C_prime_s
    delta_H_prime = 2 * sqrt(C_prime_r * C_prime_s) * sin(delta_h_prime / 2)

    rT = -2 * sqrt( (mean_C_prime ^ 7) / ((mean_C_prime ^ 7) + (25 ^ 7)) ) * sin( 60 ^ exp( -1 * ( ((mean_H_prime - 275)/25) ^ 2 ) ) ) 

    # compensations
    compensated_lightness = 1 + ( 0.015 * ((mean_L_prime - 50) ^ 2) ) / sqrt( 20 + ((mean_L_prime - 50) ^ 2) )
    compensated_chroma = 1 + 0.045 * mean_C_prime
    compensated_hue = 1 + 0.015 * mean_C_prime * theta

    delta_E = sqrt( ((delta_L_prime / (k_L * compensated_lightness)) ^ 2) + ((delta_C_prime / (k_C * compensated_chroma)) ^ 2) + ((delta_H_prime / (k_H * compensated_hue)) ^ 2) + rT * delta_C_prime * delta_H_prime / (k_C * compensated_chroma * k_H * compensated_hue) )

    return delta_E
}

function lab2lch(lab) {
    split(lab, Lab, " ")
    lightness = Lab[1]
    a_ = Lab[2]
    b_ = Lab[3]

    chroma = sqrt(a_ ^ 2 + b_ ^ 2)
    hue = atan2(b_, a_) % 360 

    return sprintf("%.4f %.4f %.4f", lightness, chroma, hue)
    }

function lch2hex(lch) {
    split(lch, Lch, " ")
    L_ = Lch[1]
    C_ = Lch[2]
    h_ = Lch[3]
    a_ = C_ * cos(h_)
    b_ = C_ * sin(h_)

    fy = (L_ + 16) / 116
    fx = a_ / 500 + fy  
    fz = fy - b_ / 200

    ke = kappa() * epsilon()
    yr = (L_ > ke) ? fy ^ 3 : L_ / kappa() 
    
    xr = reverse_infinite_slope_prevention(fx)
    zr = reverse_infinite_slope_prevention(fz)

    XYZ[1] = xr * reference_white_D65["x"]
    XYZ[2] = yr * reference_white_D65["y"]
    XYZ[3] = zr * reference_white_D65["z"]

    # XYZ -> sRGB matrix
    _R[1] = 3.2404542
    _R[2] = -1.5371385
    _R[3] = -0.4985314

    _G[1] = -0.9692660
    _G[2] = 1.8760108
    _G[3] = 0.0415560

    _B[1] = 0.0556434
    _B[2] = -0.2040259
    _B[3] = 1.0572252

    _r = dot_product(_R, XYZ)
    _g = dot_product(_G, XYZ)
    _b = dot_product(_B, XYZ)

    delete XYZ
    return lrgb2hex(_r, _g, _b)
    }

## BRIGHTNESS
# @source https://www.w3.org/TR/AERT/#color-contrast
# algorithm taken from a formula for converting RGB values to YIQ
# result is equivalent to the Y component in the YIQ color system 
#
# param rgb: RGB color
# returns number between 0 and 255 
function brightness(rgb) {
    return red(rgb) * .299 + green(rgb) * .587 + blue(rgb) * 0.114
}

## CHECK IF BRIGHTNESS DIFFERENCE
# IS SUFFICIENT
# between 2 RGB colors
# @source https://www.w3.org/TR/AERT/#color-contrast
# returns boolean
function check_brightness(rgb0, rgb1) {
    b0 = brightness(rgb0)
    b1 = brightness(rgb1)
    diff = abs(b0 - b1) 
    return (diff < 125) ? 0 : 1 
}

## RELATIVE LUMINANCE
# REC 709 recommandation for modern HDTV
# approved standard in 1990
# @source https://www.w3.org/TR/WCAG21/#dfn-relative-luminance
# 0 = darkest black
# 1 = lightest white
# see also https://www.w3.org/TR/2016/NOTE-WCAG20-TECHS-20161007/G18#G18-procedure
# see also https://en.wikipedia.org/wiki/Relative_luminance
#
# note: La luminance est la partie du signal vid�o correspondant � l'image en noir et blanc
# see also https://www.techno-science.net/definition/2975.html
# see https://modele-cie-1931.blogspot.com/2015/07/5-la-luminance-dans-le-rgb.html
function relative_luminance(rgb) {
    sR = red(rgb) / 255
    sG = green(rgb) / 255
    sB = blue(rgb) / 255  

    #r = (sR <= 0.03928) ? sR / 12.92 : ((sR + 0.055) / 1.055) ^ G
    lR = inverse_srgb_companding(sR)
    lG = inverse_srgb_companding(sG)
    lB = inverse_srgb_companding(sB)

    return max(0, min(0.2126 * lR + 0.7152 * lG + 0.0722 * lB, 1))
}

# REC 601 historical TV 1953
# why it should be better: https://poynton.ca/papers/SMPTE_98_YYZ_Luma/index.html
function lum601(rgb) {
    sR = red(rgb) / 255
    sG = green(rgb) / 255
    sB = blue(rgb) / 255  

    lR = inverse_srgb_companding(sR)
    lG = inverse_srgb_companding(sG)
    lB = inverse_srgb_companding(sB)

    return max(0, min(0.299 * lR + 0.587 * lG + 0.114 * lB, 1))
}

# CIE 1931 model 
function lum31(rgb) {
    sR = red(rgb) / 255
    sG = green(rgb) / 255
    sB = blue(rgb) / 255  

    lR = inverse_srgb_companding(sR)
    lG = inverse_srgb_companding(sG)
    lB = inverse_srgb_companding(sB)

    # https://fr.wikipedia.org/wiki/CIE_RGB#Luminance
    return max(0, min(0.17397 * lR + 0.8124 * lG + 0.01063 * lB, 1))
}

## CONTRAST RATIO 
# @source https://www.w3.org/WAI/WCAG21/Techniques/general/G17
function contrast_ratio(rgb0, rgb1) {
    lum0 = lum601(rgb0)
    lum1 = lum601(rgb1)

    lighter = max(lum0, lum1)
    darker = min(lum0, lum1)

    __c = 0.05
    ratio = (lighter + __c) / (darker + __c)

    return ratio
}

## CHECKING CONTRAST
function check_contrast(sample_color, reference_color, font_size) {
    if (WCAG_LEVEL == "AAA") 
        min_ratio = ( font_size < 24 ) ? 7 : 4.5
    else 
        min_ratio = ( font_size < 24 ) ? 4.5 : 3

    current_ratio = contrast_ratio(sample_color, reference_color)

    return (current_ratio >= min_ratio) ? 1 : 0
}

## COLOR DIFFERENCE
# @source https://www.w3.org/TR/AERT/#color-contrast
function color_diff(rgb0, rgb1) {
    r0 = red(rgb0)
    r1 = red(rgb1)
    g0 = green(rgb0)
    g1 = green(rgb1)
    b0 = blue(rgb0)
    b1 = blue(rgb1)

    minR = min(r0, r1)
    maxR = max(r0, r1)

    minG = min(g0, g1)
    maxG = max(g0, g1)

    minB = min(b0, b1)
    maxB = max(b0, b1)
    
    return (maxR - minR) + (maxG - minG) + (maxB - minB)
}

## CHECKING COLOR DIFF
function check_color_diff(rgb0, rgb1) {
    diff = color_diff(rgb0, rgb1)
    return (diff < 500) ? 0 : 1
}

function is_contrasted(samp, ref) {
    brightness_is_sufficient = check_brightness(samp, ref)
    colordiff_is_sufficent = check_color_diff(samp, ref)
    contrast_ok = check_contrast(samp, ref, 16)
    if (brightness_is_sufficient == 1 && colordiff_is_sufficent == 1 && contrast_ok == 1)
        return 1
    return 0
} 

## FIX CONTRAST
function fix_contrast(smp, ref, manip) {
    # contrast check
    sufficient = is_contrasted(smp, ref) 
    if (sufficient == 0) {
        lab_ref = hex2lab(ref)
        lch_smp = lab2lch(hex2lab(smp))
        split(lch_smp, _smp, " ")
        split(lab_ref, labr, " ")
        L_ = _smp[1]
        C_ = _smp[2]
        h_ = _smp[3]
        L_ref = labr[1]
        lums = lum601(smp)
        lumr = lum601(ref)
        if (lums == lumr) 
            lums = (lumr < 0.5) ? lums + 0.00001 : lums - 0.00001 
        # sample color is lighter
        if (manip == "lighten" && lums > lumr) {
            starter = L_ + 5
            for (counter=starter; counter<=100; counter+=5) {
                new_smp = sprintf("%.4f %.4f %.4f", min(max(0, counter), 100), C_+1, h_)
                new_smp = lch2hex(new_smp)
                sufficient = is_contrasted(new_smp, ref) 
                smp = new_smp
                if (sufficient == 1)
                    break
                }
            }
        # sample color is darker
        else if (manip == "darken" && lums < lumr) {
            starter = L_ - 5
            for (counter=starter; counter>=0; counter-=5) {
                new_smp = sprintf("%.4f %.4f %.4f", min(max(0, counter), 100), C_-1, h_)
                new_smp = lch2hex(new_smp)
                sufficient = is_contrasted(new_smp, ref) 
                smp = new_smp
                if (sufficient == 1)
                    break
                }
            }
        }
    return smp
    }

function isSameColor(samp, ref) {
    hexS = rgb2hex(samp)
    hexR = rgb2hex(ref)
    lS = hex2lab(hexS) 
    lR = hex2lab(hexR) 
    diff = dE00(lS, lR)
    return (diff <= 3.5) ? 1 : 0 
    }

# COLOR BLINDNESS SIMULATION
# ==========================
# 2 methods:
# - Jörg Dietrich's
# https://github.com/joergdietrich/daltonize/blob/master/daltonize.py
# - Jim aka hx2A
# https://ixora.io/projects/colorblindness/color-blindness-simulation-research/
function srgb2xyz(rgb) {
    r = red(rgb) / 255
    g = green(rgb) / 255
    b = blue(rgb) / 255

    # linear rgb
    r = clip(inverse_srgb_companding(r), 0, 1)
    g = clip(inverse_srgb_companding(g), 0, 1)
    b = clip(inverse_srgb_companding(b), 0, 1)

    # matrix on bruce lindbloom's website
    # https://www.brucelindbloom.com
    x = r * 0.4124564 + g * 0.3575761 + b * 0.1804375
    y = r * 0.2126729 + g * 0.7151522 + b * 0.072175
    z = r * 0.0193339 + g * 0.1191920 + b * 0.9503041

    x = clip(x, 0, 1)
    y = clip(y, 0, 1)
    z = clip(z, 0, 1)
    
    return sprintf("%.4f %.4f %.4f", x, y, z)
}
function xyz2srgb(xyz) {
    split(xyz, co, " ")
    x = co[1]
    y = co[2]
    z = co[3]

    r = x * 3.2404542 - y * 1.5371385 - z * 0.4985314
    g = y * 1.8760108 + z * 0.041556 - x * 0.969266 
    b = x * 0.0556434 - y * 0.2040259 + z * 1.0572252

    r = srgb_companding(r) * 255
    g = srgb_companding(g) * 255
    b = srgb_companding(b) * 255

    r = clip(r, 0, 255)
    g = clip(g, 0, 255)
    b = clip(b, 0, 255)

    return sprintf("rgb(%d,%d,%d)", r, g, b)
}
function xyz2lms(xyz) {
    split(xyz, co, " ")
    x = co[1]
    y = co[2]
    z = co[3]

    # HUNT-POINTER-ESTEVEZ transformation matrix
    # also called von kries matrix
    # https://en.wikipedia.org/wiki/LMS_color_space#Hunt.2C_RLAB
    l = x * 0.40024 + y * 0.7076 - z * 0.08081
    m = y * 1.16532 + z * 0.0457 - x * 0.2263
    s = z * 0.91822

    return sprintf("%.4f %.4f %.4f", l, m, s)
}
function lms2xyz(lms) {
    split(lms, co, " ")
    l = co[1]
    m = co[2]
    s = co[3]

    # reversed HUNT-POINTER-ESTEVEZ transformation matrix
    x = l * 1.8599364 - m * 1.1293816 + s * 0.2198974
    y = l * 0.3611914 + m * 0.6388125 - s * 0.0000064
    z = s * 1.0890636

    x = clip(x, 0, 1)
    y = clip(y, 0, 1)
    z = clip(z, 0, 1)

    return sprintf("%.4f %.4f %.4f", x, y, z)
}

function rgb2lms(rgb) {

    xyz = srgb2xyz(rgb)
    return xyz2lms(xyz)
}

function lms2rgb(lms) {
    xyz = lms2xyz(lms)
    return xyz2srgb(xyz)
}

function bluePrimary() {
    prim = rgb2lms(svg_color["blue"])
    split(prim, _b_, " ")
    Lb = _b_[1]
    Mb = _b_[2]
    Sb = _b_[3]
    q0 = (Lb - Sb) / (Mb - Sb)
    q1 = (Lb - Mb) / (Sb - Mb)
    return sprintf("%.4f %.4f", q0, q1) 
    }

function redPrimary() {
    prim = rgb2lms(svg_color["red"])
    split(prim, _r_, " ")
    Lr = _r_[1]
    Mr = _r_[2]
    Sr = _r_[3]
    q0 = (Sr - Mr) / (Lr - Mr)
    q1 = (Sr - Lr) / (Mr - Lr)
    return sprintf("%.4f %.4f", q0, q1) 
    }

# ERROR QUANTITIES
# diff between normal and color blind
# perception of color
# 
# contain the color information that protanope cannot see
# 
# @see https://github.com/joergdietrich/daltonize/blob/master/daltonize.py
function eq(rgb, sim) {
    diff_threshold = 10
    r = red(rgb)
    g = green(rgb)
    b = blue(rgb)
    Sr = red(sim)
    Sg = green(sim)
    Sb = blue(sim)
    modR = abs(r - Sr)
    modG = abs(g - Sg)
    modB = abs(b - Sb)
    conflicted = (modR < diff_threshold && modG < diff_threshold && modB < diff_threshold) ? 1 :0
    return sprintf("%d %d %d %d", modR, modG, modB, conflicted)
    }

function protanope(rgb) {
    col = rgb2lms(rgb)
    split(col, lms, " ")

    _m = lms[2]
    _s = lms[3]
    # simulate
    # matrix M = [
    #   0   q0  q1
    #   0   1   0
    #   0   0   1
    # ]
    # formulae of q0 and q1
    # @see https://ixora.io/projects/colorblindness/color-blindness-simulation-research/
    split(bluePrimary(),q," ")
    q0 = q[1]
    q1 = q[2]
    l = q0 * _m + q1 * _s

    plms = sprintf("%.4f %.4f %.4f", l, _m, _s)
    prgb = lms2rgb(plms)
    # color is correct
    if (isSameColor(rgb, prgb)) 
        return rgb2hex(prgb)

    pr = red(prgb)
    pg = green(prgb)
    pb = blue(prgb)

    # DALTONIZE
    # compensating loss of color information
    # by redistributing it on the other side
    # of the spectrum
    #
    # matrix M = [
    #    0   0   0
    #    .7  1   0
    #    .7  0   1
    # ]
    # 
    
    error_quantities = eq(rgb, prgb)
    split(error_quantities, EQ, " ")
    
    qr = EQ[1]
    is_conflicted = EQ[4]
    if (is_conflicted == 1) {
        while(is_conflicted == 1) {
            delete EQ
            pg += qr *.7 
            pb += qr *.7 
            pg = clip(int(pg), 0, 255)
            pb = clip(int(pb), 0, 255)
            prgb = sprintf("rgb(%d,%d,%d)", pr, pg, pb)
            error_quantities = eq(rgb, prgb)
            split(error_quantities, EQ, " ")
            is_conflicted = EQ[4]
            }
        }
    else {
        pg += qr *.7 
        pb += qr *.7 
        pg = clip(int(pg), 0, 255)
        pb = clip(int(pb), 0, 255)
        prgb = sprintf("rgb(%d,%d,%d)", pr, pg, pb)
        }
    # dec -> hexa
    return rgb2hex(prgb)
}
function deuteranope(rgb) {
    col = rgb2lms(rgb)
    split(col, lms, " ")

    _l = lms[1]
    _s = lms[3]

    # simulate
    # matrix M = [
    #   1   0   0
    #   q0  0   q1 
    #   0   0   1
    # ]
    # formulae of q0 and q1
    # @see https://ixora.io/projects/colorblindness/color-blindness-simulation-research/
    split(bluePrimary(),q," ")
    q0 = q[1]
    q1 = q[2]
    m = q0 * _l + q1 * _s

    dlms = sprintf("%.4f %.4f %.4f", _l, m, _s)
    drgb = lms2rgb(dlms)

    # color is correct
    if (isSameColor(rgb, drgb)) 
        return rgb2hex(drgb)

    dr = red(drgb)
    dg = green(drgb)
    db = blue(drgb)

    # DALTONIZE
    # compensating loss of color information
    # by redistributing it on the other side
    # of the spectrum
    #
    # matrix M = [
    #    1   .7  0
    #    0   0   0
    #    0   .7  1
    # ]
    # 
    
    error_quantities = eq(rgb, drgb)
    split(error_quantities, EQ, " ")
    
    qg = EQ[2]
    is_conflicted = EQ[4]
    if (is_conflicted == 1) {
        while(is_conflicted == 1) {
            delete EQ
            dr += qg *.7 
            db += qg *.7 
            dg = clip(int(dg), 0, 255)
            db = clip(int(db), 0, 255)
            drgb = sprintf("rgb(%d,%d,%d)", dr, dg, db)
            error_quantities = eq(rgb, drgb)
            split(error_quantities, EQ, " ")
            is_conflicted = EQ[4]
            }
        }
    else {
        dr += qg *.7 
        db += qg *.7 
        dr = clip(int(dr), 0, 255)
        db = clip(int(db), 0, 255)
        drgb = sprintf("rgb(%d,%d,%d)", dr, dg, db)
        }
    # dec -> hexa
    return rgb2hex(drgb)
}

function tritanope(rgb) {
    col = rgb2lms(rgb)
    split(col, lms, " ")

    _l = lms[1]
    _m = lms[2]

    # simulate
    # matrix M = [
    #   1   0   0
    #   0   1   0
    #   q0  q1  0 
    # ]
    # formulae of q0 and q1
    # @see https://ixora.io/projects/colorblindness/color-blindness-simulation-research/
    split(redPrimary(),q," ")
    q0 = q[1]
    q1 = q[2]
    s = q0 * _l + q1 * _m
    tlms = sprintf("%.4f %.4f %.4f", _l, _m, s)
    trgb = lms2rgb(tlms)

    # color is correct
    if (isSameColor(rgb, trgb)) 
        return rgb2hex(trgb)

    tr = red(trgb)
    tg = green(trgb)
    tb = blue(trgb)

    # DALTONIZE
    # compensating loss of color information
    # by redistributing it on the other side
    # of the spectrum
    #
    # matrix M = [
    #    1   0   .7
    #    0   1   .7
    #    0   0   0
    # ]
    #
    error_quantities = eq(rgb, trgb)
    split(error_quantities, EQ, " ")
    
    qb = EQ[3]
    is_conflicted = EQ[4]
    if (is_conflicted == 1) {
        while(is_conflicted == 1) {
            delete EQ
            tr += qb *.7 
            tg += qb *.7 
            tr = clip(int(tr), 0, 255)
            tg = clip(int(tg), 0, 255)
            trgb = sprintf("rgb(%d,%d,%d)", tr, tg, tb)
            error_quantities = eq(rgb, trgb)
            split(error_quantities, EQ, " ")
            is_conflicted = EQ[4]
            }
        }
    else {
        tr += qb *.7 
        tg += qb *.7 
        tr = clip(int(tr), 0, 255)
        tg = clip(int(tg), 0, 255)
        trgb = sprintf("rgb(%d,%d,%d)", tr, tg, tb)
        }
    # dec -> hexa
    return rgb2hex(trgb)
}


BEGIN {
    svg_color["aliceblue"]		= "#f0f8ff"
    svg_color["antiquewhite"]	= "#faebd7"
    svg_color["aqua"]   		= "#0ff"
    svg_color["aquamarine"]		= "#7fffd4"
    svg_color["azure"]  		= "#f0ffff"
    svg_color["beige"]  		= "#f5f5dc"
    svg_color["bisque"] 		= "#ffe4c4"
    svg_color["black"]  		= "#000"
    svg_color["blanchedalmond"]	= "#ffebcd"
    svg_color["blue"]	    	= "#00f"
    svg_color["blueviolet"]		= "#8a2be2"
    svg_color["brown"]  		= "#a52a2a"
    svg_color["burlywood"]		= "#deb887"
    svg_color["cadetblue"]		= "#5f9ea0"
    svg_color["chartreuse"]		= "#7fff00"
    svg_color["chocolate"]		= "#d2691e"
    svg_color["coral"]  		= "#ff7f50"
    svg_color["cornflowerblue"]	= "#6495ed"
    svg_color["cornsilk"]		= "#fff8dc"
    svg_color["crimson"]		= "#dc143c"
    svg_color["cyan"]		    = "#0ff"
    svg_color["darkblue"]		= "#00008b"
    svg_color["darkcyan"]		= "#008b8b"
    svg_color["darkgoldenrod"]	= "#b8860b"
    svg_color["darkgray"]		= "#a9a9a9"
    svg_color["darkgreen"]		= "#006400"
    svg_color["darkgrey"]		= "#a9a9a9"
    svg_color["darkkhaki"]		= "#bdb76b"
    svg_color["darkmagenta"]	= "#8b008b"
    svg_color["darkolivegreen"]	= "#556b2f"
    svg_color["darkorange"]		= "#ff8c00"
    svg_color["darkorchid"]		= "#9932cc"
    svg_color["darkred"]		= "#8b0000"
    svg_color["darksalmon"]		= "#e9967a"
    svg_color["darkseagreen"]	= "#8fbc8f"
    svg_color["darkslateblue"]	= "#483d8b"
    svg_color["darkslategray"]	= "#2f4f4f"
    svg_color["darkslategrey"]	= "#2f4f4f"
    svg_color["darkturquoise"]	= "#00ced1"
    svg_color["darkviolet"]		= "#9400d3"
    svg_color["deeppink"]		= "#ff1493"
    svg_color["deepskyblue"]	= "#00bfff"
    svg_color["dimgray"]		= "#696969"
    svg_color["dimgrey"]		= "#696969"
    svg_color["dodgerblue"]		= "#1e90ff"
    svg_color["firebrick"]		= "#b22222"
    svg_color["floralwhite"]	= "#fffaf0"
    svg_color["forestgreen"]	= "#228b22"
    svg_color["fuchsia"]       	= "#f0f"
    svg_color["gainsboro"]		= "#dcdcdc"
    svg_color["ghostwhite"]		= "#f8f8ff"
    svg_color["gold"]   		= "#ffd700"
    svg_color["goldenrod"]		= "#daa520"
    svg_color["gray"]   		= "#808080"
    svg_color["green"]  		= "#008000"
    svg_color["greenyellow"]	= "#adff2f"
    svg_color["grey"]     		= "#808080"
    svg_color["honeydew"]		= "#f0fff0"
    svg_color["hotpink"]		= "#ff69b4"
    svg_color["indianred"]		= "#cd5c5c"
    svg_color["indigo"] 		= "#4b0082"
    svg_color["ivory"]  		= "#fffff0"
    svg_color["khaki"]  		= "#f0e68c"
    svg_color["lavender"]		= "#e6e6fa"
    svg_color["lavenderblush"]	= "#fff0f5"
    svg_color["lawngreen"]		= "#7cfc00"
    svg_color["lemonchiffon"]	= "#fffacd"
    svg_color["lightblue"]		= "#add8e6"
    svg_color["lightcoral"]		= "#f08080"
    svg_color["lightcyan"]		= "#e0ffff"
    svg_color["lightgoldenrodyellow"]	= "#fafad2"
    svg_color["lightgray"]		= "#d3d3d3"
    svg_color["lightgreen"]		= "#90ee90"
    svg_color["lightgrey"]		= "#d3d3d3"
    svg_color["lightpink"]		= "#ffb6c1"
    svg_color["lightsalmon"]	= "#ffa07a"
    svg_color["lightseagreen"]	= "#20b2aa"
    svg_color["lightskyblue"]	= "#87cefa"
    svg_color["lightslategray"]	= "#778899"
    svg_color["lightslategrey"]	= "#778899"
    svg_color["lightsteelblue"]	= "#b0c4de"
    svg_color["lightyellow"]	= "#ffffe0"
    svg_color["lime"]   		= "#0f0"
    svg_color["limegreen"]		= "#32cd32"
    svg_color["linen"]  		= "#faf0e6"
    svg_color["magenta"]    	= "#f0f"
    svg_color["maroon"] 		= "#800000"
    svg_color["mediumaquamarine"]		= "#66cdaa"
    svg_color["mediumblue"]		= "#0000cd"
    svg_color["mediumorchid"]	= "#ba55d3"
    svg_color["mediumpurple"]	= "#9370db"
    svg_color["mediumseagreen"]	= "#3cb371"
    svg_color["mediumslateblue"]		= "#7b68ee"
    svg_color["mediumspringgreen"]		= "#00fa9a"
    svg_color["mediumturquoise"]		= "#48d1cc"
    svg_color["mediumvioletred"]		= "#c71585"
    svg_color["midnightblue"]	= "#191970"
    svg_color["mintcream"]		= "#f5fffa"
    svg_color["mistyrose"]		= "#ffe4e1"
    svg_color["moccasin"]		= "#ffe4b5"
    svg_color["navajowhite"]	= "#ffdead"
    svg_color["navy"]   		= "#000080"
    svg_color["oldlace"]		= "#fdf5e6"
    svg_color["olive"]  		= "#808000"
    svg_color["olivedrab"]		= "#6b8e23"
    svg_color["orange"] 		= "#ffa500"
    svg_color["orangered"]		= "#ff4500"
    svg_color["orchid"] 		= "#da70d6"
    svg_color["palegoldenrod"]	= "#eee8aa"
    svg_color["palegreen"]		= "#98fb98"
    svg_color["paleturquoise"]	= "#afeeee"
    svg_color["palevioletred"]	= "#db7093"
    svg_color["papayawhip"]		= "#ffefd5"
    svg_color["peachpuff"]		= "#ffdab9"
    svg_color["peru"]		    = "#cd853f"
    svg_color["pink"]		    = "#ffc0cb"
    svg_color["plum"]		    = "#dda0dd"
    svg_color["powderblue"]		= "#b0e0e6"
    svg_color["purple"] 		= "#800080"
    svg_color["red"]    		= "#f00"
    svg_color["rosybrown"]		= "#bc8f8f"
    svg_color["royalblue"]		= "#4169e1"
    svg_color["saddlebrown"]	= "#8b4513"
    svg_color["salmon"] 		= "#fa8072"
    svg_color["sandybrown"]		= "#f4a460"
    svg_color["seagreen"]		= "#2e8b57"
    svg_color["seashell"]		= "#fff5ee"
    svg_color["sienna"] 		= "#a0522d"
    svg_color["silver"] 		= "#c0c0c0"
    svg_color["skyblue"]		= "#87ceeb"
    svg_color["slateblue"]		= "#6a5acd"
    svg_color["slategray"]		= "#708090"
    svg_color["slategrey"]		= "#708090"
    svg_color["snow"]		    = "#fffafa"
    svg_color["springgreen"]	= "#00ff7f"
    svg_color["steelblue"]		= "#4682b4"
    svg_color["tan"]		    = "#d2b48c"
    svg_color["teal"]   		= "#008080"
    svg_color["thistle"]		= "#d8bfd8"
    svg_color["tomato"] 		= "#ff6347"
    svg_color["turquoise"]		= "#40e0d0"
    svg_color["violet"] 		= "#ee82ee"
    svg_color["wheat"]	    	= "#f5deb3"
    svg_color["white"]  		= "#fff"
    svg_color["whitesmoke"]		= "#f5f5f5"
    svg_color["yellow"]         = "#ff0"
    svg_color["yellowgreen"]	= "#9acd32"

    PROCINFO["sorted_in"] = "@val_type_asc"

    colors["bg"]["white"]       = "#fff"
    colors["bg"]["black"]       = "#000"
    colors["fg"]["white"]       = "#fff"
    colors["fg"]["black"]       = "#000"

    delta_E_threshold = 10 
    #brightness_threshold = 125
    #color_threshold = 500

    WCAG_LEVEL = "AA" # AAA | AA

    # illuminant D65 for devices
    # https://en.wikipedia.org/wiki/Illuminant_D65#Definition
    # corresponding to mid-morning daylight
    # for the supplementary 10° observer
    # more modern but less-used alternative (CIE 1964 10° Standard Observer)
    #
    # tristimulus array
    # see https://github.com/mattharrison/colorspace.py/blob/master/colorspace.py line 189
    # to real values in script (value / 100)
    reference_white_D65["x"] = 0.94811
    reference_white_D65["y"] = 1
    reference_white_D65["z"] = 1.07304

    found_bg_color = 0
    found_fg_color = 0
    found_var_color = 0
    }

