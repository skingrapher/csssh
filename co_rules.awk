@include "co.awk"
$0 ~ /@media/ {
    AT_KEYWORD_TOKEN += 1
    sub(/\{/, "", $0)
    sub(/ (only )*screen and| all and/, "", $0)
    if (match($0, /print|support/))
        next
    media_rule = trim($0)
    next
    }

$0 ~ /\}/ {
    CURLY_BRACKET_TOKEN -= 1
    if (AT_KEYWORD_TOKEN == 1 && CURLY_BRACKET_TOKEN == 0) {
        AT_KEYWORD_TOKEN = 0
        media_rule = ""
        }
    next
    }

# selectors
ruleset {
    placeholder = match($0, /::placeholder/)
    media_or_prop = match($0, /[@;]/)
    vendor = match($0, /::*-(moz|webkit|ms)-/)
    if (media_or_prop == 0 && vendor == 0 && placeholder == 0) {
        sub(/\{/, "", $0)
        selector = trim($0)
        }
    }
$0 ~ /\{/ {
    CURLY_BRACKET_TOKEN += 1
    }
$0 ~ /(background|border)-(top|bottom|left|right)-*color|color[:space:]*:|fill|stroke/ {
    split($0,f,":")
    prop = trim(f[1])
    sub(/;/, "", f[2])
    val = trim(f[2])
    sub(/[:space:]*!important/, "", val)
    # fetch properties:
    # - background
    # - background-color
    # - border-color
    # - border-top-color
    # - border-bottom-color
    # - border-left-color
    # - border-right-color
    # - fill
    # - stroke
    # - color
    if (match(prop, /(background|border)-(top|bottom|left|right)*-*color|stroke|fill/) || prop == "color" || prop == "background") {
        if (match(val, /(rgb|hsl)a*\([[:digit:]]{1,3},[[:space:]]*[[:digit:]]{1,3}%*,[[:space:]]*[[:digit:]]{1,3}%*(,[[:space:]]*[[:digit:]]*\.*[[:digit:]]+)*\)/, c)) {
            FUNCTION_TOKEN = 1
            method = c[1]
            }
        else if (match(val, /#[a-fA-F0-9]{3,6}/, c)) 
            HASH_TOKEN = 1
        if (FUNCTION_TOKEN == 1) 
            val = (method == "rgb") ? rgb2hex(c[0]) : hsl2hex(c[0])
        else if (HASH_TOKEN == 1)
            val = c[0]
        else
            next

        # shorten colors from #aabbcc to #abc. note that we want to make sure
        # cannot use backreference group in awk regexp
        val = shrtrgb(tolower(val))
        colorindex = val
        for (colorname in svg_color) {
            if (val == svg_color[colorname]) {
                colorindex = colorname
                break
                }
            }
        samp = hex2lab(val)
        is_indexed = 0
        ground = (match(prop, /back/)) ? "bg" : "fg"
        found_bg_color = (ground == "bg") ? ++found_bg_color : found_bg_color
        found_fg_color = (ground == "fg") ? ++found_fg_color : found_fg_color
        for (groundindex in colors[ground]) {
            # if color already indexed
            if (colorindex == groundindex) {
                is_indexed = 1
                colorindex = groundindex
                # no need for more check
                break
                }
            # otherwise
            ref = hex2lab(colors[ground][groundindex])
            diff = dE00(samp, ref)
            # sample is still too similar to reference color
            if (diff <= delta_E_threshold) {
                is_indexed = 1
                colorindex = groundindex
                # no need for more check
                break
                }
            }
        # color is not already indexed
        # so it is added to array
        if (is_indexed == 0)
            colors[ground][colorindex] = val

        mr = (length(media_rule) == 0) ? 0 : media_rule
        declaration_rule[mr][prop][ground][colorindex][NR] = selector
        occurences = 0
        # remove double occurences of value for same prop 
        for (ndx in declaration_rule[mr][prop][ground][colorindex]) {
            if (declaration_rule[mr][prop][ground][colorindex][ndx] == selector)
                occurences += 1
            if (occurences == 2) 
                delete declaration_rule[mr][prop][ground][colorindex][ndx]
            }
        FUNCTION_TOKEN = 0
        HASH_TOKEN = 0
        } 
    }

END {
    # black and white are not counted
    bg_map_length = length(colors["bg"]) - 2
    if (bg_map_length == 1)
        print "/* 1 indexed background color */"
    else if (bg_map_length > 1)
        printf "/* %d indexed background colors over %d */\n", bg_map_length, found_bg_color
    print "$bg:("
    show(colors["bg"])
    print ") !default;"
    fg_map_length = length(colors["fg"]) - 2
    if (fg_map_length == 1)
        print "/* 1 indexed foreground color */"
    else if (fg_map_length > 1)
        printf "/* %d indexed foreground colors over %d */\n", fg_map_length, found_fg_color
    print "$fg:("
    show(colors["fg"])
    print ") !default;"

    # if func returns null, rule will not be compiled by Sass
    print "@function getColor($color, $palette)"
    print "{"
    print "\t@return if(map-has-key($color), map-get($palette, $color), null);"
    print "}"
    print "@mixin bg($col, $prop: 'background-color', $map: $bgcolor)"
    print "{\n\t$c: getcolor($col, $map);"
    print "\t@if not($c == null) {#{$prop}: $c !important;}\n}"
    print "@mixin fg($col, $prop: 'color', $map: $fgcolor)"
    print "{\n\t$c: getcolor($col, $map);"
    print "\t@if not($c == null) {#{$prop}: $c !important;}\n}"

    for (mr in declaration_rule) {
        prefix = (mr == 0) ? "" : sprintf("%s\n{\n", mr)
        buffer = ""
        for (ground in colors) {
            for (colorindex in colors[ground]) {
                _c = colors[ground][colorindex]
                for (property in declaration_rule[mr]) {
                    _p = ""
                    if (ground == "bg" && match(property, "background-color") == 0)
                        _p = sprintf(",'%s'", property)
                    if (ground == "fg" && match(property, "color") == 0)
                        _p = sprintf(",'%s'", property)

                    for (gr in declaration_rule[mr][property]) {
                        if (gr == ground) {
                            for (ci in declaration_rule[mr][property][ground]) {
                                if (ci == colorindex) {
                                    for (ruleindex in declaration_rule[mr][property][ground][colorindex]) {
                                        selector = declaration_rule[mr][property][ground][colorindex][ruleindex]
                                        selector = sprintf("%s,", selector)
                                        sub(/,+/, ",", selector)
                                        if (mr == 0)
                                            buffer = sprintf("%s%s\n", buffer, selector) 
                                        else
                                            buffer = sprintf("%s\t%s\n", buffer, selector)
                                        delete declaration_rule[mr][property][ground][colorindex][ruleindex]
                                        }
                                    if (mr == 0)
                                        buffer = sprintf("%s{\n\t@include %s('%s'%s);\n}\n", buffer, ground, _c, _p)
                                    else
                                        buffer = sprintf("%s\t{\n\t\t@include %s('%s'%s);\n\t}\n", buffer, ground, _c, _p)
                                    }
                                }
                            }
                        }
                    }
                } 
            }
        suffix = (mr == 0) ? "" : "}\n"
        if (length(buffer) > 0)
            printf "%s%s%s", prefix, buffer, suffix
        }
    }

