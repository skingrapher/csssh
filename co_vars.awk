@include "co.awk"
$0 ~ /@media/ {
    AT_KEYWORD_TOKEN += 1
    sub(/\{/, "", $0)
    sub(/ (only )*screen and| all and/, "", $0)
    if (match($0, /print|support/))
        next
    media_rule = trim($0)
    next
    }

$0 ~ /\}/ {
    CURLY_BRACKET_TOKEN -= 1
    if (AT_KEYWORD_TOKEN == 1 && CURLY_BRACKET_TOKEN == 0) {
        AT_KEYWORD_TOKEN = 0
        media_rule = ""
        }
    next
    }

# selectors
$0 ~ /(:root|:scope|:target|a|abbr|address|area|article|aside|audio|b|base|bdi|bdo|blockquote|body|br|button|canvas|caption|cite|code|col|colgroup|datalist|dd|del|details|dfn|div|dl|dt|em|embed|fieldset|figcaption|figure|footer|form|h1|header|hgroup|hr|html|i|iframe|img|input|ins|kbd|keygen|label|legend|li|map|mark|menu|meta|meter|nav|noscript|object|ol|optgroup|option|output|p|param|pre|progress|q|rp|rt|ruby|s|samp|section|select|small|source|span|strong|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|u|ul|var|video|wbr)[$,\s>+~\{\.#:\[]|([\.#]\w+([-_]+\w+)*)+(::*(after|before|cue|checked|default|disabled|enabled|first-letter|first-line|focus|hover|indeterminate|in-range|invalid|link|optional|out-of-range|read-only|read-write|required|valid|visited))*[$,#\.\s>+~\{\[]|(input|button)*(\[type=["']*(button|checkbox|color|date|datetime-local|email|file|image|month|number|password|radio|range|reset|search|submit|tel|text|time|url|week)["']*\])+|:not\(|(first|last|nth|nth-last)-(child|of-type)/ {
    placeholder = match($0, /::placeholder/)
    media_or_prop = match($0, /[@;]/)
    vendor = match($0, /::*-(moz|webkit|ms)-/)
    if (media_or_prop == 0 && vendor == 0 && placeholder == 0) {
        sub(/\{/, "", $0)
        selector = trim($0)
        }
    }
$0 ~ /\{/ {
    CURLY_BRACKET_TOKEN += 1
    }
$0 ~ /--[:alpha:]+[:space:]?:[:space:]+[#rh]/ {
    split($0,f,":")
    prop = trim(f[1])
    sub(/;/, "", f[2])
    val = trim(f[2])
    sub(/[:space:]*!important/, "", val)
    if (match(val, /(rgb|hsl)a*\([[:digit:]]{1,3},[[:space:]]*[[:digit:]]{1,3}%*,[[:space:]]*[[:digit:]]{1,3}%*(,[[:space:]]*[[:digit:]]*\.*[[:digit:]]+)*\)/, c)) {
        FUNCTION_TOKEN = 1
        method = c[1]
        }
    else if (match(val, /#[a-fA-F0-9]{3,6}/, c)) 
        HASH_TOKEN = 1
    if (FUNCTION_TOKEN == 1) 
        val = (method == "rgb") ? rgb2hex(c[0]) : hsl2hex(c[0])
    else if (HASH_TOKEN == 1)
        val = c[0]
    else
        next

    # shorten colors from #aabbcc to #abc. note that we want to make sure
    # cannot use backreference group in awk regexp
    val = shrtrgb(tolower(val))
    colorindex = val
    for (colorname in svg_color) {
        if (val == svg_color[colorname]) {
            colorindex = colorname
            break
            }
        }
    samp = hex2lab(val)
    is_indexed = 0
    ground = "var" 
    found_var_color++
    for (groundindex in colors[ground]) {
        # if color already indexed
        if (colorindex == groundindex) {
            is_indexed = 1
            colorindex = groundindex
            # no need for more check
            break
            }
        # otherwise
        ref = hex2lab(colors[ground][groundindex])
        diff = dE00(samp, ref)
        # sample is still too similar to reference color
        if (diff <= delta_E_threshold) {
            is_indexed = 1
            colorindex = groundindex
            # no need for more check
            break
            }
        }
    # color is not already indexed
    # so it is added to array
    if (is_indexed == 0)
        colors[ground][colorindex] = val

    FUNCTION_TOKEN = 0
    HASH_TOKEN = 0
    }

END {
    # black and white are not counted
    var_map_length = length(colors["var"])
    if (var_map_length == 1)
        print "/* 1 indexed var color */"
    else if (bg_map_length > 1)
        printf "/* %d indexed var colors over %d */\n", var_map_length, found_var_color
    if (var_map_length > 0) {
        print "$vars:("
        show(colors["var"])
        print ") !default;"

        print "@each $k, $v in $vars\n{\n\t#{$k}: $v !important;\n}"
        }
    }

