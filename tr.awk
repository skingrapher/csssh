#!/usr/bin/awk -f

#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright © 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
@include "trim.awk"
@include "selector.awk"

BEGIN {
    CURLY_BRACKET_TOKEN = 0
    AT_KEYWORD_TOKEN = 0
    IDENT_TOKEN = 0

    # if var is declared from command line
    if (LETTER_SPACING) 
        LETTER_SPACING = LETTER_SPACING
    else
        LETTER_SPACING = 0 

    if (WORD_SPACING) 
        WORD_SPACING = WORD_SPACING
    else
        WORD_SPACING = 0 

    PROCINFO["sorted_in"] = "@val_type_asc"

    }
$0 ~ /@media/ {
    AT_KEYWORD_TOKEN += 1
    sub(/\{/, "", $0)
    sub(/ (only )*screen and| all and/, "", $0)
    media_rule = trim($0)
    }

# selectors
ruleset {
    placeholder = match($0, /::placeholder/)
    media_or_prop = match($0, /[@;]/)
    vendor = match($0, /::*-(moz|webkit|ms)-/)
    if (media_or_prop == 0 && vendor == 0 && placeholder == 0) {
        sub(/\{/, "", $0)
        selector = trim($0)
        IDENT_TOKEN = 1
        }
    }
$0 ~ /\{/ {
    CURLY_BRACKET_TOKEN += 1
    }
$0 ~ /(letter|word)-spacing/ {
    if (IDENT_TOKEN == 1) {
        split($0,f,":")
        prop = trim(f[1])

        mr = (length(media_rule) == 0) ? 0 : media_rule
        declaration_rule[mr][prop][NR] = selector
        occurences = 0
        # remove double occurences of value for same prop 
        for (ndx in declaration_rule[mr][prop]) {
            if (declaration_rule[mr][prop][ndx] == selector)
                occurences += 1
            if (occurences == 2) 
                delete declaration_rule[mr][prop][ndx]
            }

        IDENT_TOKEN = 0
        }
    }

$0 ~ /\}/ {
    CURLY_BRACKET_TOKEN -= 1
    if (AT_KEYWORD_TOKEN == 1 && CURLY_BRACKET_TOKEN == 0) {
        AT_KEYWORD_TOKEN = 0
        media_rule = ""
        }
    }

END {
    printf "$letter-spacing: %d;\n", LETTER_SPACING 
    printf "$word-spacing: %d;\n", WORD_SPACING  

    for (mr in declaration_rule) {
        prefix = (mr == 0) ? "" : sprintf("%s\n{\n", mr)
        buffer = ""
        for (property in declaration_rule[mr]) {
            val = (property == "letter-spacing") ? LETTER_SPACING : WORD_SPACING
            val = (val == 0) ? "normal" : val
            for (ndx in declaration_rule[mr][property]) {
                selector = declaration_rule[mr][property][ndx]
                selector = sprintf("%s,", selector)
                sub(/,+/, ",", selector)
                if (mr == 0)
                    buffer = sprintf("%s%s\n", buffer, selector) 
                else
                    buffer = sprintf("%s\t%s\n", buffer, selector)
                delete declaration_rule[mr][property][ndx]
                }
            if (mr == 0)
                buffer = sprintf("%s{\n\t%s: %s !important;\n}\n", buffer, property, val)
            else
                buffer = sprintf("%s\t{\n\t\t%s: %s !important;\n\t}\n", buffer, property, val)
            } 
        suffix = (mr == 0) ? "" : "}\n"
        if (length(buffer) > 0)
            printf "%s%s%s", prefix, buffer, suffix
        }
    }
