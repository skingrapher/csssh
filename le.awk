#!/usr/bin/awk -f

#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright © 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
@include "trim.awk"
@include "selector.awk"

BEGIN {
    CURLY_BRACKET_TOKEN = 0
    AT_KEYWORD_TOKEN = 0
    IDENT_TOKEN = 0

    if (LINE_HEIGHT) 
        LINE_HEIGHT = LINE_HEIGHT
    else
        LINE_HEIGHT = 1.5
    lh[0] = LINE_HEIGHT

    PROCINFO["sorted_in"] = "@val_type_asc"
    }
$0 ~ /@media/ {
    AT_KEYWORD_TOKEN += 1
    sub(/\{/, "", $0)
    sub(/ (only )*screen and| all and/, "", $0)
    media_rule = trim($0)
    }

# selectors
ruleset {
    placeholder = match($0, /::placeholder/)
    media_or_prop = match($0, /[@;]/)
    vendor = match($0, /::*-(moz|webkit|ms)-/)
    if (media_or_prop == 0 && vendor == 0 && placeholder == 0) {
        sub(/\{/, "", $0)
        selector = trim($0)
        IDENT_TOKEN = 1
        }
    }
$0 ~ /\{/ {
    CURLY_BRACKET_TOKEN += 1
    }
$0 ~ /line-height/ {
    if (IDENT_TOKEN == 1) {
        split($0,f,":")
        prop = trim(f[1])
        sub(/;/, "", f[2])
        val = trim(f[2])
        sub(/[:space:]*!important/, "", val)

        if (match(val, /r*em/)) {
            sub(/r*em/, "", val)
            }
        # https://www.w3.org/TR/css3-values/#absolute-lengths
        else if (match(val, /pt|pc|px/)) {
            val = LINE_HEIGHT
            }
        else if (val == 0) 
            next
        else 
            next
        
        is_indexed = 0
        for (sz in lh) {
            if (val == lh[sz]) {
                is_indexed = 1
                break
                }
            }
        if (is_indexed == 0)
            lh[NR] = val

        mr = (length(media_rule) == 0) ? 0 : media_rule
        declaration_rule[mr][val][NR] = selector
        occurences = 0
        # remove double occurences of value for same prop 
        for (ndx in declaration_rule[mr][val]) {
            if (declaration_rule[mr][val][ndx] == selector)
                occurences += 1
            if (occurences == 2) 
                delete declaration_rule[mr][val][ndx]
            }

        IDENT_TOKEN = 0
        }
    }

$0 ~ /\}/ {
    CURLY_BRACKET_TOKEN -= 1
    if (AT_KEYWORD_TOKEN == 1 && CURLY_BRACKET_TOKEN == 0) {
        AT_KEYWORD_TOKEN = 0
        media_rule = ""
        }
    }

END {
    printf "$line-height: %g;\n", LINE_HEIGHT
    print "@mixin lh($lh;$base:$line-height){"
    # fixed line height values other than em, are overrided
    print "\t@if unitless($lh) or unit($lh) == em { line-height: #{if($lh>$base, $lh, $base)} !important; }"
    print "\t@else { line-height: $line-height !important; }"
    print "}"
    for (mr in declaration_rule) {
        prefix = (mr == 0) ? "" : sprintf("%s\n{\n", mr)
        buffer = ""
        for (sz in lh) {
            size = lh[sz]
            for (val in declaration_rule[mr]) {
                if (val == size) {
                    for (ndx in declaration_rule[mr][val]) {
                        selector = declaration_rule[mr][val][ndx]
                        selector = sprintf("%s,", selector)
                        sub(/,+/, ",", selector)
                        if (mr == 0)
                            buffer = sprintf("%s%s\n", buffer, selector) 
                        else
                            buffer = sprintf("%s\t%s\n", buffer, selector)
                        delete declaration_rule[mr][val][ndx]
                        }
                    if (mr == 0)
                        buffer = sprintf("%s{\n\t@include lh(%s);\n}\n", buffer, size)
                    else
                        buffer = sprintf("%s\t{\n\t\t@include lh(%s);\n\t}\n", buffer, size)
                    }
                } 
            }
        suffix = (mr == 0) ? "" : "}\n"
        if (length(buffer) > 0)
            printf "%s%s%s", prefix, buffer, suffix
        }
    }
