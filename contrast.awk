#!/usr/bin/awk -f

#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright � 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
@include "co.awk"
BEGIN {
    print "// CONTRAST ENHANCEMENT"

    if (REF)
        REF = REF
    else
        REF = svg_color["white"]
    if (MANIP)
        MANIP = (MANIP == "lighten") ? "lighten" : "darken"
    else
        MANIP = "darken"
    }
$0 ~ /^\$[fb]g:/ {
    print
    }
$0 ~ /:[[:space:]]*#/ {
    split($0,c,":")
    key = trim(c[1])
    val = trim(c[2])
    printf("\t%s:%s,\n",key,fix_contrast(val, REF, MANIP))
    }
$0 ~ /^\);/ {
    print $0 "\n"
    }
