#!/usr/bin/awk -f

#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright � 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
@include "trim.awk"
@include "selector.awk"

BEGIN {
    selectors[0] = ""
    PROCINFO["sorted_in"] = "@val_type_asc"
    }
# selectors
ruleset {
    no_placeholder = match($0, /::placeholder/)
    no_media_no_prop = match($0, /[@;]/)
    no_vendor = match($0, /::*-(moz|webkit|ms)-/)
    if (no_media_no_prop == 0 && no_vendor == 0 && no_placeholder == 0) {
        sub(/\{/, "", $0)
        selector = trim($0)
        }
    else
        next
    }
$0 ~ /height[[:space:]]?:/ {
    if (match($0, /[enx]-height/))
        next

    # no img element
    else if (match(selector, / img,*$| img[\.#]|:after|:before/)) 
        next
    else {
        selector = sprintf("%s,", selector)
        sub(/,+/, ",", selector)
        for (ndx in selectors) 
            if (selector == selectors[ndx])
                next
            
        selectors[NR] = selector
        }
    }

END {
    delete selectors[0]
    if (length(selectors) > 0) {
        for (str in selectors)
            print selectors[str]
        print "{"
        print "\theight: auto !important;"
        print "}"
        }
    }

