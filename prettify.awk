#!/usr/bin/awk -f
#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright © 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
# adaptation for awk from
# https://github.com/vkiryukhin/vkBeautify/blob/master/vkbeautify.js
function prettify(text, step) {
    # argument is string 
	if ( strtonum(step) == 0)
        space = step
    # argument is integer
	else {
        s = " "
        space = sprintf("%*s", step - 1, s)
        } 

    # array of shifts
    shift[0] = "\n"
	for(i=0;i<100;i++){
        j = i + 1
        shift[j] = sprintf("%s%s", shift[i], space) 
        }

    gsub(/[[:blank:]]+/, " ", text)
    gsub(/\{/, "{~::~", text)
    gsub(/\}/, "~::~}~::~", text)
    gsub(/;/, ";~::~", text)
    gsub(/\/\*/, "~::~/*", text)
    gsub(/\*\//, "*/~::~", text)
    gsub(/~::~[[:space:]]*~::~/, "~::~", text)
    split(text, ar, "~::~")
    len = length(ar)
    deep = 0
    str = ""
        
    for (ix=0;ix<len;ix++) {
        if (match(ar[ix], /\{/)) {
            str = sprintf("%s%s%s", str, shift[deep++], ar[ix])
            }
        else if (match(ar[ix], /\}/))
            str = sprintf("%s%s%s", str, shift[--deep], ar[ix])
        else
            str = sprintf("%s%s%s", str, shift[deep], ar[ix])
        }

    sub(/^\n+/, "", str)
    print str
    }

BEGIN {
    step = "\t"
    }
{
    prettify($0, step)
    }


