#!/usr/bin/awk -f

#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright � 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
@include "trim.awk"
@include "selector.awk"

function round(n) {
    if (n < 0)
        return (n - (N = int(n)) < -.5) ? N - 1 : N
    else
        return (n - (N = int(n)) >= .5) ? N + 1 : N
    }

function resizefont(currentsize, base, rootsize, rem) {
    # convertible units
    if (match(currentsize, /px|r*em|p[ctx]|%/)) {
        if (match(currentsize, /em/)) {
            sub(/r*em/, "", currentsize)
            currentsize = int(currentsize)
            size = round(currentsize * rem)
            }
        else if (match(currentsize, /pc/)) {
            sub(/pc/, "", currentsize)
            currentsize = int(currentsize)
            size = round(currentsize * 16)
            }
        else if (match(currentsize, /pt/)) {
            sub(/pt/, "", currentsize)
            currentsize = int(currentsize)
            size = round(currentsize * 4 / 3)
            }
        else if (match(currentsize, /px/)) {
            sub(/px/, "", currentsize)
            currentsize = int(currentsize)
            size = round(currentsize)
            }
        else if (match(currentsize, /%/)) {
            sub(/%/, "", currentsize)
            currentsize = int(currentsize)
            size = round(currentsize * rootsize / 100)
            }
        # never less than 9px
        # note 1 on https://www.w3.org/TR/css-fonts-3/#relative-size-value
        size = (size < 9) ? 9 : size
        return (size < rootsize) ? rootsize : (size < base) ? base : size 
        }
    # unitless
    else if (match(currentsize, /[[:digit:]]+/)) 
        return (int(fs) > base) ? fs : base
    else
        return base 
    }

BEGIN {
    # if var is declared from command line
    if (ROOT_FONT_SIZE) 
        ROOT_FONT_SIZE = ROOT_FONT_SIZE
    else
        ROOT_FONT_SIZE = 16

    if (CUSTOM_FONT_SIZE) 
        CUSTOM_FONT_SIZE = CUSTOM_FONT_SIZE
    else
        CUSTOM_FONT_SIZE = 18

    if (REM_SIZE) 
        REM_SIZE = REM_SIZE
    else
        REM_SIZE = 16 

    fs[0] = ROOT_FONT_SIZE  

    PROCINFO["sorted_in"] = "@val_type_asc"
    }
$0 ~ /@media/ {
    AT_KEYWORD_TOKEN += 1
    sub(/\{/, "", $0)
    sub(/ (only )*screen and| all and/, "", $0)
    media_rule = trim($0)
    }

# selectors
ruleset {
    placeholder = match($0, /::placeholder/)
    media_or_prop = match($0, /[@;]/)
    vendor = match($0, /::*-(moz|webkit|ms)-/)
    if (media_or_prop == 0 && vendor == 0 && placeholder == 0) {
        sub(/\{/, "", $0)
        selector = trim($0)
        IDENT_TOKEN = 1
        }
    }
$0 ~ /\{/ {
    CURLY_BRACKET_TOKEN += 1
    }
$0 ~ /font-size/ {
    if (IDENT_TOKEN == 1) {
        split($0,f,":")
        sub(/;/, "", f[2])
        val = trim(f[2])
        sub(/[:space:]*!important/, "", val)

        # https://www.w3.org/TR/css-fonts-3/#absolute-size-value
        xxsm = sprintf("%dpx", round(CUSTOM_FONT_SIZE * 3 / 5))
        xsm = sprintf("%dpx", round(CUSTOM_FONT_SIZE * 3 / 4))
        sm = sprintf("%dpx", round(CUSTOM_FONT_SIZE * 8 / 9))
        medium = sprintf("%dpx", round(CUSTOM_FONT_SIZE))
        large = sprintf("%dpx", round(CUSTOM_FONT_SIZE * 6 / 5))
        xl = sprintf("%dpx", round(CUSTOM_FONT_SIZE * 3 / 2))
        xxl = sprintf("%dpx", round(CUSTOM_FONT_SIZE * 2))
        xxxl = sprintf("%dpx", round(CUSTOM_FONT_SIZE * 3))
        sub(/xx-small/, xxsm, val)
        sub(/x-small/, xsm, val)
        sub(/small/, sm, val)
        sub(/medium/, medium, val)
        sub(/large/, large, val)
        sub(/x-large/, xl, val)
        sub(/xx-large/, xxl, val)
        sub(/xxx-large/, xxxl, val)

        if (match(val, /r*em|pt|%|pc|px/) == 0)
            next

        if (val == 0) 
            next
        
        is_indexed = 0
        for (sz in fs) {
            if (val == fs[sz]) {
                is_indexed = 1
                break
                }
            }
        if (is_indexed == 0)
            fs[NR] = val

        mr = (length(media_rule) == 0) ? 0 : media_rule
        declaration_rule[mr][val][NR] = selector
        occurences = 0
        # remove double occurences of value for same prop 
        for (ndx in declaration_rule[mr][val]) {
            if (declaration_rule[mr][val][ndx] == selector)
                occurences += 1
            if (occurences == 2) 
                delete declaration_rule[mr][val][ndx]
            }

        IDENT_TOKEN = 0
        }
    }

$0 ~ /\}/ {
    CURLY_BRACKET_TOKEN -= 1
    if (AT_KEYWORD_TOKEN == 1 && CURLY_BRACKET_TOKEN == 0) {
        AT_KEYWORD_TOKEN = 0
        media_rule = ""
        }
    }

END {
    print "@mixin fs($fs){ font-size: #{resize-font($fs)}px !important; }\n"
    
    for (mr in declaration_rule) {
        prefix = (mr == 0) ? "" : sprintf("%s\n{\n", mr)
        buffer = ""
        for (sz in fs) {
            size = fs[sz]
            for (val in declaration_rule[mr]) {
                if (val == size) {
                    for (ndx in declaration_rule[mr][val]) {
                        selector = declaration_rule[mr][val][ndx]
                        selector = sprintf("%s,", selector)
                        sub(/,+/, ",", selector)
                        if (mr == 0)
                            buffer = sprintf("%s%s\n", buffer, selector) 
                        else
                            buffer = sprintf("%s\t%s\n", buffer, selector)
                        delete declaration_rule[mr][val][ndx]
                        }
                    #size = resizefont(size, CUSTOM_FONT_SIZE, ROOT_FONT_SIZE, REM)
                    if (mr == 0)
                        #buffer = sprintf("%s{\n\tfont-size: %spx !important;\n}\n", buffer, size)
                        buffer = sprintf("%s{\n\t@include fs(%s);\n}\n", buffer, size)
                    else
                        buffer = sprintf("%s\t{\n\t\t@include fs(%s);\n\t}\n", buffer, size)
                    }
                } 
            }
        suffix = (mr == 0) ? "" : "}\n"
        if (length(buffer) > 0)
            printf "%s%s%s", prefix, buffer, suffix
        }
    }
