BEGIN {
    CURLY_BRACKET_TOKEN = 0
    AT_KEYWORD_TOKEN = 0
    IDENT_TOKEN = 0

    _A_         = "a|\\0{0,4}(41|61)(\r\n|[ \t\r\n\f])?"
    _C_         = "c|\\0{0,4}(43|63)(\r\n|[ \t\r\n\f])?" 
    _D_         = "d|\\0{0,4}(44|64)(\r\n|[ \t\r\n\f])?"
    _E_         = "e|\\0{0,4}(45|65)(\r\n|[ \t\r\n\f])?"
    _G_         = "g|\\0{0,4}(47|67)(\r\n|[ \t\r\n\f])?|\\g"
    _I_         = "i|\\0{0,4}(49|69)(\r\n|[ \t\r\n\f])?|\\i"
    _K_         = "k|\\0{0,4}(4b|6b)(\r\n|[ \t\r\n\f])?|\\k"
    _M_         = "m|\\0{0,4}(4d|6d)(\r\n|[ \t\r\n\f])?|\\m"
    _N_         = "n|\\0{0,4}(4e|6e)(\r\n|[ \t\r\n\f])?|\\n" 
    _P_         = "p|\\0{0,4}(50|70)(\r\n|[ \t\r\n\f])?|\\p" 
    _R_         = "r|\\0{0,4}(52|72)(\r\n|[ \t\r\n\f])?|\\r"
    _S_         = "s|\\0{0,4}(53|73)(\r\n|[ \t\r\n\f])?|\\s"
    _T_         = "t|\\0{0,4}(54|74)(\r\n|[ \t\r\n\f])?|\\t" 
    _X_         = "x|\\0{0,4}(58|78)(\r\n|[ \t\r\n\f])?|\\x"
    _Z_         = "z|\\0{0,4}(5a|7a)(\r\n|[ \t\r\n\f])?|\\z"
    nonascii    = "[\240-\377]"
    unicode     = "\\{h}{1,6}(\r\n|[ \t\r\n\f])?"
    escape      = unicode "|\\[^\r\n\f0-9a-f]"
    nmstart     = "[_a-z]|" nonascii "|" escape
    nmchar      = "[_a-z0-9-]|" nonascii "|" escape
    ident       = "-?" nmstart nmchar "*"
    name        = nmchar "+"
    hash        = "#" name
    class       = "." ident
    _s          = "[ \t\r\n\f]+"
    includes    = "~="
    dashmatch   = "|=" 
    str1        = sprintf("\"([^\n\r\f\"]|\\%s|%s)*\"", nl, escape)
    str2        = sprintf("'([^\n\r\f\']|\\%s|%s)*'", nl, escape) 
    str_        = str1 "|" str2 
    nl          = "\n|\r\n|\r|\f"
    num         = "[-+]?[0-9]+|[-+]?[0-9]*\.[0-9]+" 
    percent     = num "%" 
    _len        = num "("_C_ _M_ "|" _M_ _M_"|" _I_ _N_ "|" _P_ _T_ "|" _P_ _C_ ")" 
    ems         = num _E_ _X_
    exs         = num _E_ _M_
    angle       = num "(" _D_ _E_ _G_ "|" _R_ _A_ _D_ "|" _G_ _R_ _A_ _D_ ")" 
    _time       = num "(" _M_ _S_"|" _S_ ")" 
    freq        = num "(" _H_ _Z_ "|" _K_ _H_ _Z_ ")" 
    nb          = num "(/\\|/-|/-\\|+/\.)" 
    term        = "(" nb _s "*|" percent _s "*|" _len _s "*|" ems _s "*|" exs _s "*|" angle _s "*|" _time "*|" freq _s ")|" str_ _s "*|" ident _s "*|" uri _s "*|" hexcolor 
    _ope        = "/" _s "*|," _s "*"
    expr        = term "(" _ope "?" term ")"
    _func       = ident "(" _s "*" expr ")" _s "*"
    term        = term "*|" _func
    pseudo      = ":(" ident "|" _func _s "*(" ident _s "*)?\))"
    attrib      = "[" _s "*" ident _s "*" "((=|" includes "|" dashmatch ")" _s "*(" ident"|" str_ ")" _s "*)?]"
    element_name    = ident "|\*"
    combinator  = "+" _s "*|>" _s "*"  
    simple_selector = element_name "(" hash "|" class "|" attrib "|" pseudo ")*|(" hash "|" class "|" attrib "|" pseudo")+"
    selector    = simple_selector "(" combinator selector "|" _s "+(" combinator "?" selector ")?)?"
    ruleset     = selector "(," _s "*" selector ")*\{" _s "*" declaration "?(;" _s "*" declaration"?)*\}" _s "*"
    }
