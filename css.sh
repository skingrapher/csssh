#!/usr/bin/env bash

#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright © 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
# VERSION
#
# 2020-11-06: 0.9.0
# 2021-03-13: 0.9.1
#   - added selector grammar as regexp pattern 
#

BSD=$(uname -s|grep -e BSD -c)
# DEFAULT BOOLEAN OPTIONS
FAMILY=
DALTONIZE=
LINE_HEIGHT=
CSS_FILTER_PROP=
CSS_ALL=
CSS_FILE=
PRETTY=
MINI=
REF=
REM=16
ROOT_FS=
LETTER_SPACING=
WORD_SPACING=
USER_AGENT="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"

if [ $BSD == 1 ]; then
    AWK=gawk
else
    AWK=awk
fi
function _prettify() {
    if [ ! -z $MINI ] ; then
    $AWK -f prettify.awk $MINI 1> _${MINI%%.*}.pretty.scss
    echo "prettified format of $MINI has been saved in _${MINI%%.*}.pretty.scss"
    fi
} 
function _minify() {
    if [ ! -z $PRETTY ] ; then
    $AWK -f minify.awk $PRETTY 1> _${PRETTY%%.*}.min.scss
    echo "minified format of $PRETTY has been saved in _${PRETTY%%.*}.min.scss"
    fi
} 
# FILTERING PROPERTIES
function _fs() {
    if [ ! -z $CSS_FILE ] ; then
        if [ -z $ROOT_FS ]; then
            ROOT_FS=$($AWK -f root_font_size.awk $CSS_FILE)
        fi
        if [ -z $CUSTOM_FS ]; then
            if [ ! -z $REM ]; then
                $AWK -v ROOT_FONT_SIZE=$ROOT_FS -v REM=$REM -f fs.awk $CSS_FILE 1> _${CSS_FILE%%.*}.FS.scss
            else
                $AWK -v ROOT_FONT_SIZE=$ROOT_FS -f fs.awk $CSS_FILE 1> _${CSS_FILE%%.*}.FS.scss
            fi
        else
            if [ ! -z $REM ]; then
                $AWK -v ROOT_FONT_SIZE=$ROOT_FS -v CUSTOM_FONT_SIZE=$CUSTOM_FS -v REM=$REM -f fs.awk $CSS_FILE 1> _${CSS_FILE%%.*}.FS.scss
            else
                $AWK -v ROOT_FONT_SIZE=$ROOT_FS -v CUSTOM_FONT_SIZE=$CUSTOM_FS -f fs.awk $CSS_FILE 1> _${CSS_FILE%%.*}.FS.scss
            fi
        fi
        echo "font size styles have been listed in _${CSS_FILE%%.*}.FS.scss"
    fi
}
function _co() {
    if [ ! -z $CSS_FILE ] ; then
        IS_CO=$( echo $CSS_FILE |grep -e "CO.scss" -c )
        if [ $IS_CO == 0 ]; then
            $AWK -f co_rules.awk $CSS_FILE 1> _${CSS_FILE%%.*}.CO.scss
            echo "color styles have been listed in _${CSS_FILE%%.*}.CO.scss"
        fi

        TEMP_FILE=$(basename $CSS_FILE CO.scss | sed 's/^_//;s/\./.scss/')
        cp $CSS_FILE $TEMP_FILE
        CSS_FILE=$TEMP_FILE

        if [ -z $REF ] ; then
            echo "referent color for contrast: white"
            $AWK -f contrast.awk $CSS_FILE 1> _${CSS_FILE%%.*}.CO.contrast.scss
        else
            echo "referent color for contrast: " $REF
            $AWK -f contrast.awk -v REF=$REF $CSS_FILE 1> _${CSS_FILE%%.*}.CO.contrast.scss
        fi
        echo "contrasted colors styles have been listed in _${CSS_FILE%%.*}.CO.scss"

        rm $TEMP_FILE

        if [ ! -z $DALTONIZE ] ; then
            $AWK -f prt.awk _${CSS_FILE%%.*}.CO.contrast.scss > _${CSS_FILE%%.*}.CO.prt.scss
            echo "daltonized colors for PROTANOPIA have been listed in _${CSS_FILE%%.*}.CO.prt.scss"
            $AWK -f dtr.awk _${CSS_FILE%%.*}.CO.contrast.scss > _${CSS_FILE%%.*}.CO.dtr.scss
            echo "daltonized colors for DEUTERANOPIA have been listed in _${CSS_FILE%%.*}.CO.dtr.scss"
            $AWK -f trt.awk _${CSS_FILE%%.*}.CO.contrast.scss > _${CSS_FILE%%.*}.CO.trt.scss
            echo "daltonized colors for TRITANOPIA have been listed in _${CSS_FILE%%.*}.CO.trt.scss"
        fi
    fi
}

function _ff() {
    if [ ! -z $CSS_FILE ] ; then
        if [ ! -z $FAMILY ] ; then
            $AWK -f ff.awk -v FONT_FAMILY=$FAMILY $CSS_FILE 1> _${CSS_FILE%%.*}.FF.scss
        else
            $AWK -f ff.awk $CSS_FILE 1> _${CSS_FILE%%.*}.FF.scss
        fi
        echo "font family styles have been listed in _${CSS_FILE%%.*}.FF.scss"
    fi
}

function _le() {
    if [ ! -z $CSS_FILE ] ; then
        if [ ! -z $LINE_HEIGHT ] ; then
            $AWK -f le.awk -v LINE_HEIGHT=$LINE_HEIGHT $CSS_FILE 1> _${CSS_FILE%%.*}.LE.scss
        else
            $AWK -f le.awk $CSS_FILE 1> _${CSS_FILE%%.*}.LE.scss
        fi

        echo "leading rules have been listed in _${CSS_FILE%%.*}.LE.scss"
    fi
}
function _tr() {
    if [ ! -z $CSS_FILE ] ; then
        if [ -z $LETTER_SPACING && -z $WORD_SPACING ] ; then
            $AWK -f tr.awk $CSS_FILE 1> _${CSS_FILE%%.*}.TR.scss

        elif [ ! -z $LETTER_SPACING && -z $WORD_SPACING ] ; then
            $AWK -f tr.awk -v LETTER_SPACING=$LETTER_SPACING $CSS_FILE 1> _${CSS_FILE%%.*}.TR.scss

        elif [ -z $LETTER_SPACING && ! -z $WORD_SPACING ] ; then
            $AWK -f tr.awk -v WORD_SPACING=$WORD_SPACING $CSS_FILE 1> _${CSS_FILE%%.*}.TR.scss

        else 
            $AWK -f tr.awk -v LETTER_SPACING=$LETTER_SPACING -v WORD_SPACING=$WORD_SPACING $CSS_FILE 1> _${CSS_FILE%%.*}.TR.scss
        fi
        echo "tracking rules have been listed in _${CSS_FILE%%.*}.TR.scss"
    fi
}
function _h() {
    if [ ! -z $CSS_FILE ] ; then
        $AWK -f h.awk $CSS_FILE 1> _${CSS_FILE%%.*}.H.scss
        echo "height rules have been listed in _${CSS_FILE%%.*}.H.scss"
    fi
}


# HELP
function _help() {
    cat <<EOF
#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# SYNOPSYS
#  css.sh -p file.min.css
#  css.sh -m file.css 
#  css.sh -i css_file -o property [parameters]
#  css.sh -i file.css -a 
#  css.sh -h
#
# OPTIONS
#  -p css_min_file  prettify minified css file
#  -m css_file      minify css file
#  -i css_file      input pretty css file before manipulation
#  -o property      one css property (see list below)
#
#  -a               all properties with default parameters (see below)
#
# PROPERTY 
#
#  fs   font size
#  ff   font family
#  tr   tracking
#  le   leading
#  co   colors
#  h    height
#
# OPTIONAL PARAMETERS
#
## font size
# css.sh -o fs [-r integer] [-c integer] [-e integer] -i file.css
#  -r   font size at root of the document
#       unitless number in pixels
#       automatically detected in css file if declared
#       default: 16
#  -c   font sizes less than custom will be scaled to custom
#       default: 20
#  -e   value of 1rem in pixels
#       default: 16
#
## font family 
# css.sh -o ff [-f string] -i file.css
#  -f   font family
#
## tracking
# css.sh -o tr [-l string] [-w string] -i file.css
#  -l   letter spacing value with unit
#  -w   word spacing value with unit
#
#       default for both: 0
#
## leading
# css.sh -o le [-h float] -i file.css 
#  -H   line height value (unitless) 
#       default: 1.5
#
## colors
# css.sh -o co [-r color] [-d] -i file.css 
#  -R   referent color for contrast enhancement
#       colors are contrasted in rapport with referent color
#       default: white
#  -d   daltonize
#       colors are contrasted before daltonization
#  -i   can be a file.CO.scss (see output)
#
## height
#  no parameter
#
# OUTPUT
#
# file.FS.scss
# file.FF.scss
# file.TR.scss
# file.LE.scss
# file.CO.scss
# file.CO.contrasted.scss
# file.CO.prt.scss (daltonized colors for protanopia)
# file.CO.dtr.scss (daltonized colors for deuteranopia)
# file.CO.trt.scss (daltonized colors for tritanopia)
# file.H.scss
#
EOF
}

while getopts ac:de:f:H:hi:l:m:o:p:R:r:w: option
do
    case "${option}"
        in
        a) CSS_ALL=1;;
        c) CUSTOM_FS=${OPTARG};;
        d) DALTONIZE=1;;
        e) REM=${OPTARG};;
        f) FAMILY=${OPTARG};;
        H) LINE_HEIGHT=${OPTARG};;
        h) _help;;
        i) CSS_FILE=${OPTARG};;
        l) LETTER_SPACING=${OPTARG};;
        m) PRETTY=${OPTARG};;
        o) CSS_FILTER_PROP=${OPTARG};;
        p) MINI=${OPTARG};;
        R) REF=${OPTARG};;
        r) ROOT_FS=${OPTARG};;
        w) WORD_SPACING=${OPTARG};;
    esac
done

if [ ! -z $MINI ] ; then
    _prettify 

elif [ ! -z $PRETTY ] ; then
    _minify 

elif [ ! -z $CSS_FILTER_PROP ] ; then
    case $CSS_FILTER_PROP
        in
        fs) _fs;;
        ff) _ff;;
        tr) _tr;;
        co) _co;;
        le) _le;;
        h) _h;;
    esac

elif [ ! -n $CSS_ALL ] ; then
    _fs && _ff && _h && _tr &&_co && _le
fi
