#!/usr/bin/awk -f

#   ____ ____ ____   ____  _   _ 
#  / ___/ ___/ ___| / ___|| | | |
# | |   \___ \___ \ \___ \| |_| |
# | |___ ___) |__) | ___) |  _  |
#  \____|____/____(_)____/|_| |_|
#                                
# script to fetch css files from html page,
# filter some typographic and color property rules
# and modify them
#
# copyright © 2018 skingrapher 
# 
# This file is part of csssh.
# 
# csssh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#
@include "trim.awk"

BEGIN {
    CLOSED_RULE = 0
    selector = ""
    root_font_size = 16
    rfs["body"] = root_font_size 
    }

$0 ~ /\{/ { 
    CLOSED_RULE = 0
    }

$0 ~ /body|html|root/ { 
    selector = trim($0)
    }

$0 ~ /\}/ { 
    CLOSED_RULE = 1
    selector = ""
    }


$0 ~ /font-size/ {
        if (CLOSED_RULE == 0) {
            if (selector == "body" || selector == "html" || selector == "root") {
                split($0,f,":")
                sz = f[2]
                gsub(/[[:space:]]/,"",sz)
                sub(";","",sz)
                if (match(sz, /r*em/)) {
                    sub(/r*em/, "", sz)
                    sz = root_font_size * int(sz)
                    }
                else if (match(sz, /%/)) {
                    sub(/%/, "", sz)
                    sz = root_font_size * int(sz) / 100 
                    sz = int(sz)
                    }
                # https://www.w3.org/TR/css3-szues/#absolute-lengths
                else if (match(sz, /pt/)) {
                    sub(/pt/, "", sz)
                    sz = int(sz) * 4 / 3 
                    sz = int(sz)
                    }
                else if (match(sz, /pc/)) {
                    sub(/pc/, "", sz)
                    sz = int(sz) * 16 
                    }
                else if (match(sz, /px/)) {
                    sub(/px/, "", sz) 
                    sz = int(sz)
                    }
                else 
                    sz = root_font_size

                rfs[selector] = sz 
                } 
            }
    }

END {
    fs = rfs["root"] ? rfs["root"] : rfs["body"]
    fs = rfs["html"] ? rfs["html"] : fs
    fs = rfs["body"] != 16 ? rfs["body"] : fs 
    print fs
    }
